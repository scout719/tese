\chapter{Work Plan}
\label{cha:work_plan}
The goal of this thesis is to provide a mechanism to efficiently offload compound computations to the Intel\textregistered\ Xeon Phi\texttrademark. The work will build upon the existing Marrow framework. Accordingly, we begin in Section \ref{sec:marrow} with an overview of the framework, pointing out some characteristics and explaining its programming and execution models. Section \ref{sec:plan} elaborates on the work that we plan to carry out in the context of this dissertation. Section \ref{sec:eval} presents our evaluation methodology. Lastly, the distribution of work planned for the time available for this dissertation is presented in Section \ref{sec:sched}.

\section{Marrow}
\label{sec:marrow}
Marrow \cite{marrow13, marrow-sac2014} is a $C$++ ASkF (\textbf{A}lgorithmic \textbf{Sk}eleton \textbf{F}ramework) for general purpose computing in multiple GPU machines. It provides a library of skeletons useful for task- and data-parallelism, with the possibility of skeleton nesting. Furthermore, it transparently performs a number of optimizations, such as the overlap of computation with communication to reduce the overhead of data transfers between the host and the GPU (which is usually carried out over the PCIe bus). The skeletons orchestrate the details of the programming model leaving only the need to provide the parallel computations, in the form of OpenCL\texttrademark\ kernels.

A structured computation is represented by a tree of skeletons where the root node is the one who manages all the required resources resources, including their allocation; the inner nodes are skeletons; and, the leaf nodes encapsulate the actual computation kernels. The framework currently supports five skeletons:

\begin{description}
\item[\texttt{Pipeline}] It is used to apply multiple computations to the input data in a way such that the output of a stage is the input to the next stage. This skeleton is particularly suitable for accelerator computing, in general, because the intermediate results can be kept on the accelerator's memory, disregarding the overhead of transferring them to the host.

\item[\texttt{For} and \texttt{Loop}] This skeleton iterativelly applies an OpenCL\texttrademark\ computation, being that the loop's control condition may, or not, depend on data computed by the nested kernel. The former is akin to a while loop, while the second is closer to a for. In fact, due to its wide use, the \texttt{For} specialization of the \texttt{Loop} skeleton is featured in the framework's skeleton library. Besides, there are still two modes of execution in a multi GPU environment, one is when each iteration needs the data modified by previous iterations requiring the existence of a synchronization point at the end of each iteration, the other is when the iterations are independent so all GPUs can execute their work without waiting for the remainder devices.

\item[\texttt{Map} and \texttt{MapReduce}] When a computation is to be applied to independent partitions of the input data the \texttt{Map} skeleton should be used. In case of, at the end of applying the computation to all partitions, the multiple results need to be aggregated, the \texttt{MapReduce} skeleton is the one to be used.
With the exception of the \texttt{Map} and \texttt{MapReduce} that must be root of the tree, all other skeletons support nesting meaning that can be used as inner nodes enabling the composition of more complex computations.
\end{description}

\lstinputlisting[float, language=marrow, caption=Image filter pipeline using the Marrow framework \cite{marrow-sac2014}, label={lst:marrow_example}]{source/marrow_example.cpp}

\paragraph{Programming Example} Listing \ref{lst:marrow_example} illustrates a simple programming example that builds a pipeline with three stages. The programming model itself comprises three stages: allocation of the computation tree, execution requests and de-allocation of the tree. In the allocation stage (lines 2 to 7), the leaf nodes representing the kernels must be encapsulated in instances of the \texttt{KernelWrapper} class, along with the types of the input and output data of each kernel. The tree is constructed by composing multiple skeletons and kernels with the option to specify the number of GPUs to use on each node where the configuration used is the one defined by the root node. Also, requests can be issued to inner nodes, being their configuration the one that is used. Before the execution being issued, the input data must be enclosed in a \texttt{Vector} object. Execution requests (lines 9 to 11), prompt through the write routine, return \texttt{future} objects. Hence synchronization is data-centric, host and skeleton execution may run concurrently until the former requires the result output by the latter. After all the work is done the computation tree must be de-allocated in order to free all the resources needed. The simple de-allocation of the tree's root eliminates all intermediate nodes.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=.6\textwidth]{marrow-arch}
	\caption{Marrow's architecture}
	\label{fig:marrow-arch}
\end{figure}

\paragraph{Architecture} Marrow's architecture, depicted in Figure \ref{fig:marrow-arch}, comprises two layers: the skeleton library and the runtime module. The programmer only has access to the library layer that provides the multiple skeletons, the \texttt{Vector} concept that encapsulates all data transfer operations, the \texttt{KernelWrapper}, and the kernel data types to wrap the kernels in order to be used by the framework. The runtime layer is responsible for dealing with all the details inherent to the management of OpenCL\texttrademark\ computations, namely problem decomposition across multiple devices, scheduling, and communication (including synchronization). The \texttt{Scheduler} module is used to decide the distribution of partitions among all the available GPUs using the \texttt{Auto-Tuner} module to decompose the domain of a kernel. The transfer of data to a device and the consequent execution of the kernel is dealt by the \texttt{Task Launcher} module. The \texttt{ExecutionPlatform} module is the main responsible for interacting with the underlying platform. In the case of the OpenCL\texttrademark\ devices, is this module that manages, for example, the command queues associated with each device. Recently the support for multiple \texttt{ExecutionPlatform} modules was added so the computation tree can be executed exclusively, or in parallel, by multiple platforms.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=.6\textwidth]{marrow-exec}
	\caption{Marrow's execution model}
	\label{fig:marrow-exec}
\end{figure}

\paragraph{Execution Model} Marrow’s internal execution model is depicted in Figure \ref{fig:marrow-exec}. The root of the tree starts by creating a \texttt{future} object and returns it to the application (step 1). Then, the task is submitted to the \texttt{Scheduler} so that the work can be distributed among the target devices (step 2). The resulting partitions are subsequently scheduled to each device, as they are pushed unto per-device work queues (step 3). When possible, the \texttt{TaskLauncher} removes a task from a queue (step 4), and requests to the \texttt{ExecutionPlatform} the transfer of data to the device and the execution of the associated computation (step 5). After the task finishes and when all output data is available to be used, the \texttt{TaskLauncher} notifies the future object associated to the computation that the task is finished (step 6).

\section{Planning}
\label{sec:plan}
The objective of this thesis is to leverage  the characteristics of the Marrow framework in the programming of the Xeon Phi\texttrademark\ processor. To accomplish such enterprise our approach will comprise several steps. 
The first is to assess which technology, of the ones described in Section \ref{sec:frameworks}, delivers better performances (in general) when applied to the Xeon Phi\texttrademark\ context. 

Posteriorly, given that Marrow is an ASkF for the orquestration of OpenCL computations, and there is already an implementation of OpenCL\texttrademark\ for the Xeon Phi\texttrademark, our next step will be to evaluate the performance of the current version of the framework when targeted at the co-processor. For that purpose we will build upon the framework's modularity to add the specialized support for the co-processor by implementing a new \texttt{ExecutionPlatform} module that will try to take most out of the co-processor's architecture.

We would like our work to go beyond the use of OpenCL\texttrademark\ and be able to leverage the optimizations performed by the Intel\textregistered\ compiler (or others). However, currently the Marrow framework is very coupled with the OpenCL\texttrademark\ technology, since it was first developed to work with GPUs. To overcome this limitation, the framework will be augmented with support for multiple back-ends for different technologies, adding then the back-end for the technology chosen. A key change resulting from this adaptation is that the kernels provided to the framework no longer have to be OpenCL\texttrademark\ specific but instead, can be written in $C$/$C$++. One of the challenges to be considered is the communication management. All communication between the nodes of the tree and inside the skeletons must be generated by the framework. There are some aspects that can be explored to increase efficiency, like the possibility to run multiple kernels at the same time, as opposing to the GPU that can only execute one kernel at a time. Also, in order for the tree's leafs, the computation, to fully extract the optimizations performed by the Intel\textregistered\ compiler, explicit code annotations will have to be inserted. We want these to be encapsulated by the framework, but we will have to further investigate how this can be done.

This new version of the framework will be evaluated from both the performance and productivity perspective, against both the previous OpenCL\texttrademark\ version and hand-written $C$++ applications.

Currently, the framework only supports the discrete execution of computations. We would also like to give the support for execution over contiguous flows of data (streams). The challenges of this mechanism are to understand the amount of data to send periodically as well as when to send it in order to provide an efficient execution.

\section{Evaluation}
\label{sec:eval}
%o que é que queremos avaliar, que perguntas queremos responder?
%	que ganhos (speedup) conseguimos com o offloading das computações para o xeon phi?
%		openmp vs opencl
%	como é que estes se comparam com ganhos em gpus?
%	que ganhos se obtêm com alguma optimização em particular que tenha sido feita?
%
%como é que o vamos fazer?
%	já existem algumas aplicações programadas para o marrow, iremos medir essas (em OpenCL) 
%	para openmp teremos de adaptá-las
%	prevemos que sejam implementadas novas aplicações, baseados em benchmarks  existentes como o NPB
%
%modelo de programação
%	comparar com quê?
%		contra OpenCL (já está feita)					
%		a segunda versão  - compara versus offload + openmp + cilk ??

The effectiveness of the solutions proposed in the scope of this thesis will be mostly evaluated from a performance perspective. 

The first question we would like to answer is ``Is the Xeon Phi competitive against current GPU and CPU for the execution of complex OpenCL computations orchestrated by Marrow?''.
To that end we will evaluate the OpenCL\texttrademark\ version, by  comparing the Xeon Phi\texttrademark\ version against the original version running on multiple GPUs and even on the CPU. The metric that will be used to assess the performance will be the speedup obtained from the OpenCL\texttrademark\ version. For this purpose we will resort to the benchmark applications currently developed for Marrow, and eventually implement new ones.

The next question will be ``How does the new version of Marrow compare to the OpenCL\texttrademark\ version?''. To answer this, the speedup achieved by the new version will be measured against the previous one. Again, Marrow's testing suite will be used but now, since the framework interface will be changed, the available examples will need to be rewritten in order to match the new interface.

Since one of the objectives of this work will be to efficiently execute the computations on the Xeon Phi\texttrademark, all code related with computations executed on the co-processor, will be optimized. So, the gains resulting from such optimization will be measured throughout the development of the solution.

The last question will be ``How does the new version of Marrow compare against hand-written applications?''. For that, we will evaluate our solution from a performance and productivity point of view. In order to do it, some common HPC applications will be implemented, from the NPB (\textbf{N}AS \textbf{P}arallel \textbf{B}enchmarks), using only the technologies available to program for the Xeon Phi\texttrademark\ and then, the same applications, will be changed to use the framework. After that the size of both source codes will be compared to analyze the effort required to use our solution.

\section{Schedule}
\label{sec:sched}
The planned work was divided into multiple stages that were then scheduled to the time available for this thesis, shown in Figure \ref{fig:schedule}. The stages defined are the following:

\begin{enumerate}
\item \textbf{Technology choice} - In this stage, the multiple technologies available will be tested on the co-processor to assess its performance in order to choose one to be used for the back-end implementation stage.
\item \textbf{Runtime implementation} - Since the co-processor supports the OpenCL\texttrademark\ technology, in this stage an \texttt{ExecutionPlatform} will be implemented so that the framework can interact with the co-processor.
\item \textbf{Adaptation of framework specification} -  In order to allow a different technology to be used the framework will modified accordingly. This stage is the biggest one because of the need to change the internals of the framework.
\item \textbf{Back-end implementation} - When the framework is ready to support more technologies a back-end for the one chosen on the first stage will be created.
\item \textbf{Streaming implementation} - After the framework is completely functional, working on the co-processor, the feature for streaming data to the computation tree will be implemented.
\item \textbf{Evaluation} - To assess the quality of the work developed, the multiple versions of the framework will be tested and compared for its performance and ease of use at the end of each implementation stage.
\item \textbf{Dissertation} - The final stage will correspond to the interpretation of the results obtained from the previous stage and then the writing of the dissertation.
\end{enumerate}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=.6\textwidth]{schedule}
	\caption{Scheduling of work}
	\label{fig:schedule}
\end{figure}