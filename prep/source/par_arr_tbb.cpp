size_t N = 1024;
size_t STRIDE = 20;
float a[N];
float b[N];
atomic<int> res;

void dot_range( const blocked_range<size_t> &r ){
     for( size_t i = r.begin(); i != r.end(); ++i )
         res += a[i]*b[i];
}

void dot_array(){
	parallel_for(blocked_range<size_t>(0, N, STRIDE), dot_range, 
			auto_partitioner());
}