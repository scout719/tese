#include <stdio.h>
#include <cilk/cilk.h>

void Recurse(const int task) {
	if (task < 10) {
		printf("Creating task %d...", task+1);
		cilk_spawn Recurse(task+1);
		long foo=0; for (long i = 0; i < (1L<<20L); i++) foo+=i;
		printf("result of task %d in worker %d is %ld\n", task,
			__cilkrts_get_worker_number(), foo);
	}
}

int main() {
	Recurse(0);
}