// Computation tree allocation ; Define partitionable argument
inArgs [0] = shared_ptr <IWorkData>( new BufferData <cl_uchar4>( 
				imgWidth*imgHeight, IWorkData::PARTITIONABLE, LINE_SIZE ));
unique_ptr <IExecutable> gaussKernel( new KernelWrapper( 
				gaussKernelSrc, "gaussian", inArgs, outArgs, workSize ));
unique_ptr <IExecutable> p1( new Pipeline( gaussKernel, solariseKernel ));
unique_ptr <IExecutable> p2( new Pipeline( p1, mirrorKernel, 2 )); //2 GPUs
// Request skeleton executions
inputVectors[0] = shared_ptr <Vector>( 
				new Vector( input, sizeof(cl_uchar4), imgWidth*imgHeight ));
future = p2->write( inputVectors, outputVectors ); // Execution request
// Wait for results ; Computation tree de - allocation : delete p2