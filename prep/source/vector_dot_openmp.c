#include <omp.h>
#include <stdio.h>

int main (int argc, char* argv[]){
	int i, n, chunk;
	n = 1000;
	chunk = 10;
	int a[n], b[n], result, result_check;
	result = result_check = 0.0;

	/* Some initializations */
	for (i=0; i < n; i++){
		a[i] = i; b[i] = i * 2;
	}

#pragma omp parallel for 	\
	schedule(static,chunk)	\
	reduction(+:result)
	for (i=0; i < n; i++)
		result = result + (a[i] * b[i]);

	for(i = 0; i < n; i++)
		result_check += (a[i] * b[i]);
	printf("Final result = %d (should be %d)\n",result, result_check);
	return 0;
}

