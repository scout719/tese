__declspec(vector(linear(a),uniform(b)))
void bar(float* a, float* b, int c, int d);

void foo(float* a, float* b, int* c, int* d, int n){
#pragma simd
	for( int i=0; i<n; ++i )
		bar( a+i, b, c[i], d[i] );
}