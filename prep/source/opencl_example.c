//Initialization
cl_int err;
cl_context context;
cl_device_id devices;
cl_command_queue cmd_queue;
err = clGetDeviceIDs(CL_DEVICE_TYPE_GPU, 1, &devices, NULL);
context = clCreateContext(0, 1, &devices, NULL, NULL, &err);
cmd_queue = clCreateCommandQueue(context, devices, 0, NULL);
//Allocation
cl_mem ax_mem = clCreateBuffer(context, CL_MEM_READ_ONLY,
		atom_buffer_size, NULL, NULL);
err = clEnqueueWriteBuffer(cmd_queue, ax_mem, CL_TRUE, 0,
		atom_buffer_size, (void*)ax, 0,NULL,NULL);
//Program/Kernel creation
cl_program program[1];
cl_kernel kernel[1];
program[0] = clCreateProgramWithSource(context,1, 
		(const char**)&program_source, NULL, &err);
err = clBuildProgram(program[0], 0, NULL, NULL, NULL, NULL);
kernel[0] = clCreateKernel(program[0], "mdh", &err);
//Execution
err = clSetKernelArg(kernel[0], 0, sizeof(cl_mem), &ax_mem);
err = clEnqueueTask(cmd_queue, kernel[0],
		0, NULL, NULL);
//Finalization
err = clEnqueueReadBuffer(cmd_queue, val_mem, CL_TRUE, 0,
		grid_buffer_size, val, 0, NULL, NULL);
clReleaseKernel(kernel);
clReleaseProgram(program);
clReleaseCommandQueue(cmd_queue);
clReleaseContext(context);