const int N = 4086;
float *f1, *f2;
f1 = (float *)memalign(64, N*sizeof(float)); 
f2 = (float *)memalign(64, N*sizeof(float));

// CPU sends f1 as input synchronously
// The output is in f2, but is not needed immediately
#pragma offload target (mic:0) \
					in(  f1 : length(N) ) \
					nocopy( f2 : length(N) free_if(0)) signal(f2)
{
	foo(N, f1, f2);
}

// Perform some computation asynchronously

#pragma offload_transfer (mic:0) wait(f2) \
						out( f2 : length(N) alloc_if(0) free_if(1))
// CPU can now use the result in f2