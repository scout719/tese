float cilkplus_sprod(
  size_t n,
  const float a[],
  const float b[] ) {
	return __sec_reduce_add(a[0:n] * b[0:n]);
}