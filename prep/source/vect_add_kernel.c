__kernel void arr_add(
  __global const float *a,
  __global const float *b, 
  __global float *c,
  const unsigned int nelems)
{
	int id = get_global_id(0);
	if(id < nelems)
		c[id] = a[id] * b[id];
} 