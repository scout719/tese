#include <omp.h>
#include <stdio.h>

void Recurse(const int task_) {
	if (task_ < 10) {
		printf("Creating task %d...", task_+1);
#pragma omp task
		{
			Recurse(task_+1);
		}
		unsigned long foo=0; for (long i = 0; i < (1<<16); i++) foo+=i;
		printf("result of task %d in thread %d is %ld\n", task_, omp_get_thread_num(), foo);
	}
}

int main(int argc, char* argv[]) {
#pragma omp parallel
	{
#pragma omp single
		Recurse(0);
	}
	return 0;
}