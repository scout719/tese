#include <stdio.h>
#include <stdlib.h>
#include <new>

class _Cilk_shared MyClass {
		int i;
	public:
		MyClass(){ i = 0; };
		void set(const int l) { i = l; }
		void print(){
			printf("Value: %d\n", i);
		}
};

MyClass* _Cilk_shared sharedData;

int main(){
	const int size = sizeof(MyClass);
	_Cilk_shared MyClass* address = (_Cilk_shared MyClass*) _Offload_shared_malloc(size);
	sharedData=new( address ) MyClass;
	sharedData->set(1000); // Shared data initialized on host
	_Cilk_offload sharedData->print(); // Shared data used on coprocessor
	sharedData->print(); // Shared data used on host
}