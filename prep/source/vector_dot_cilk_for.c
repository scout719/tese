float cilkplus_sprod(
  size_t n,
  const float a[],
  const float b[] ) {
	cilk::reducer_opadd<float> res(0);
	cilk_for (size_t i = 0; i < n; i++){
		res += a[i] * b[i];
	}
	return res.get_value();
}